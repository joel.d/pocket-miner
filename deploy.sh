#!/bin/bash

# Check if root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 5
fi

# Check bash version
if { echo "$BASH_VERSION"; echo "4.2"; } | sort --version-sort --check=silent
then
echo "This version of Bash is not compatible with this script. Kindly upgrade the Bash version."
exit 10
fi

# Verify OS version
OS=$(grep '^ID=' /etc/os-release | cut -f 2 -d '=')
OS_VERSION=$(grep 'VERSION_ID=' /etc/os-release | cut -f 2 -d '"')

if [[ $OS == ubuntu ]]
then
:
else
echo "This script was made to run on Ubuntu systems only."
exit 15
fi

if { echo "$OS_VERSION"; echo "20.04"; } | sort --version-sort --check=silent
then
echo "This script will run on Ubuntu 20.04 and above."
exit 20
fi


# Checking if all required files are present as per original

TMPMD5SUM="/tmp//md5sum"
find . -type f | sort | grep -vE 'md5sum|git' | xargs md5sum | sed -e 's/\.\///g' > $TMPMD5SUM

if [[ ! -f ./md5sum ]]
then
echo "The original md5sum file does not exist. Exiting immediately."
exit 25
fi

for MINERFILE in `cat $TMPMD5SUM | awk '{print $2}'`
do
ORIG_MD5=`grep $MINERFILE ./md5sum`
CUR_MD5=`grep $MINERFILE $TMPMD5SUM`

if [ "$ORIG_MD5" != "$CUR_MD5" ]
then
echo "Error: The md5sum for $MINERFILE does not match the original value. This could be trouble during further deployment. Exiting here for safety"
exit 50
fi 

done

# Check if CPU and memory is sufficient

if [[ $(nproc) -lt 2 ]]
then
echo "Error: The number of available CPUs is less than the required 2 CPUs. Please increase the CPUs in the system before going ahead"
exit 75
fi

if [[ $(cat /proc/meminfo | grep MemAvailable | awk '{print $2}') -lt 1048576 ]]
then
echo "Error: The amount of available memory on this system is less than 1GB. The docker containers require at least 1GB to deploy and run."
exit 100
fi

# Identify PWD

echo "Info: Seems like I am running from within $PWD. The free space in this directory is $(df -Ph . | awk '{print $4}' | tail -n 1)."
MINERWD=$(echo $PWD)
if [[ $(df -P . | awk '{print $4}' | tail -n 1) -lt 1048576 ]]
then
echo "Warning: The disk space in this folder seems to be low. Consider increasing the disk space to at least 1GB as webchaind generates a lot of logs or switch to a different directory at the next prompt."
DISKFLAG=0
elif [[ $(df -P . | awk '{print $4}' | tail -n 1) -lt 104857 ]]
then
echo "Error: The disk space in this folder is lower than 100MB. This directory cannot be used and needs to be switched."
DISKFLAG=1
else
echo "This disk space in this directory seems adequate."
fi

# Set Working Directory
dir_location_selection () {
read -p "Do you wish to switch the working directory to another location [ YES/NO ] ? " DIRCHOICE

case "$DIRCHOICE" in

[nN] | [nN][Oo] )
    echo "Proceding with $MINERWD as the working directory."
    unset $DIRCHOICE
     ;;

[yY] | [yY][Ee][Ss] )
    OLDMINERWD=$MINERWD
    unset $MINERWD
    read -p "Enter a new location. If the location does not exist, it will be created: " MINERWD
    MINERWD=$(echo ${MINERWD}/pocket-miner)
    if [[ "$MINERWD" =~ [^A-Za-z0-9./_-] ]]
    then 
    echo "Kindly ensure that the directory name contains valid charactes (Alphabets, Numbers, Hyphens."
    fi
    mkdir -p $MINERWD && cd $MINERWD
    if [[ $(df -P . | awk '{print $4}' | tail -n 1) -lt 104857 ]]
    then
    echo "Error: The new location does not have adequate disk space. Please re-run this script after freeing up at least 1GB for this project."
    exit 250
    fi
    unset $DIRCHOICE
    ;;

*) echo "Please select either YES or NO"
    ;;

esac
}

if [[ -z ${DIRCHOICE+x} ]]
then
dir_location_selection
fi

echo $OLDMINERWD
echo $MINERWD

# Set Directory Paths

if [[ ! -z ${OLDMINERWD+x} ]]
then 
cp -r ${OLDMINERWD}/* $MINERWD
fi

# Modify code to match new path
cd $MINERWD
sed -e "s,/opt/miner/docker,$MINERWD,g" -i docker-compose.yml


# Check if docker is installed as per our needs

if [[ -z $(which docker) ]]
then
echo "Seems like docker isn't installed on this system."
DOCKER_EXEC=0
else
if docker compose version
then
echo "The docker compose plugin seems to be ready to use."
DOCKER_EXEC=1
else
echo "Though Docker is installed the docker compose plugin isn't available on this system. Kindly install the appropriate docker plugin for your docker environment and run this script again."
exit 200
fi
fi

if [[ $DOCKER_EXEC -eq 0 ]]
then 

# Confirm and begin Docker-CE deployment
install_choice () {
read -p "Begin installation of Docker and its dependencies ? [ Yes/No ] " INSTALL_CHOICE

echo $INSTALL_CHOICE

case "$INSTALL_CHOICE" in

[yY] | [yY][Ee][Ss] )
    echo "Proceeding with installation."
    DOCKER_INSTALL=1
     ;;
     
[nN] | [nN][Oo] )
    echo "Exiting now. Re-run again once docker and docker-compose-plugin is available on this system."
    DOCKER_INSTALL=2
    exit 300
     ;;
     
*) echo "Please select either YES or NO"
    ;;

esac
}

if [[ -z ${DOCKER_INSTALL+x} ]]
then
install_choice
fi


# Docker Installation
sleep 5 && echo "Beginning installation"

# Add Docker repos and keys to the system

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

  
# Update system packages 
DEBIAN_FRONTEND=noninteractive
apt update && apt -y upgrade 
apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates git docker-ce docker-ce-cli containerd.io docker-compose docker-compose-plugin

# Implement system changes

modprobe overlay
modprobe br_netfilter
echo 'overlay
br_netfilter' >> /etc/modules


echo "net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.ipv6.conf.all.disable_ipv6=1
net.ipv6.conf.default.disable_ipv6=1" >> /etc/sysctl.conf

sysctl --system


# Setup storage options for Docker

echo '{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}' >> /etc/docker/daemon.json

DOCKER_EXEC=1
fi


# Check Docker environment 
if [[ DOCKER_EXEC -eq 1 ]]
then
if [[ $(df -P /var/lib/docker | awk '{print $4}' | tail -n 1) -lt 10485760 ]]
then
echo "Please ensure that there is at least 10GB free space available at /var/lib/docker for the docker build steps and image storage."
exit 350
fi
fi

# Build the docker images
docker compose config

sleep 5

docker compose build
sleep 5
# Run the docker containers

docker compose up -d
