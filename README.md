# pocket-miner



## Getting started

This project was started to create a portable mining application with its built-in pool and its management UI. 

The Mintme-com code was selected to create this application. The final installed application will contain 3 parts;
1. The webchaind Mintme-com Coin Daemon
2. The Mintme-com Mining Pool
3. The Mintme-com Miner 


## System Requirements

CPU: 2 cores or vCPUs
Memory: 4GB 
Disk: 10GB free space (to build and run the docker images and also to store a few logs)  

## Building and Compiling

Simply download the code and run the ```deploy.sh``` script to configure your system and, build and run the entire application.

Alternatively, you may install docker along with the docker compose plugin and then simply edit the docker-compose.yml to your actual path and then execute ```docker compose up -d ```

## Usage

Please take a look at the [MintMe project](https://github.com/mintme-com) over at GitHub for details regarding individual components and their usage.

## Acknowledgment
The code used in this project was created by the MintMe project.
